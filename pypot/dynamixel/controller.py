from ..robot.controller import MotorsController


class DxlController(MotorsController):
    """ Synchronizes the reading/writing of :class:`~pypot.dynamixel.motor.DxlMotor` with the real motors.

        This class handles synchronization loops that automatically read/write values from the "software" :class:`~pypot.dynamixel.motor.DxlMotor` with their "hardware" equivalent. Those loops shared a same :class:`~pypot.dynamixel.io.DxlIO` connection to avoid collision in the bus. Each loop run within its own thread as its own frequency.

        .. warning:: As all the loop attached to a controller shared the same bus, you should make sure that they can run without slowing down the other ones.

        """
    def __init__(self, io, motors, controllers):
        MotorsController.__init__(self, io, motors, 1.)
        self.controllers = controllers

    def setup(self):
        """ Starts all the synchronization loops. """
        [c.start() for c in self.controllers]
        [c.wait_to_start() for c in self.controllers]

    def update(self):
        pass

    def teardown(self):
        """ Stops the synchronization loops. """
        [c.stop() for c in self.controllers]


class BaseDxlController(DxlController):
    """ Implements a basic controller that synchronized the most frequently used values.

    More precisely, this controller:
        * reads the present position, speed, load at 50Hz
        * writes the goal position, moving speed and torque limit at 50Hz
        * writes the pid gains (or compliance margin and slope) at 10Hz
        * reads the present voltage and temperature at 1Hz

    """
    def __init__(self, io, motors):
        factory = _DxlRegisterController

        controllers = [_PosSpeedLoadDxlController(io, motors, 50),
                       factory(io, motors, 10, 'set', 'pid_gain', 'pid'),
                       factory(io, motors, 10, 'set', 'compliance_margin'),
                       factory(io, motors, 10, 'set', 'compliance_slope'),
                       factory(io, motors, 1, 'get', 'angle_limit'),
                       factory(io, motors, 1, 'get', 'present_voltage'),
                       factory(io, motors, 1, 'get', 'present_temperature')]

        DxlController.__init__(self, io, motors, controllers)


class _DxlController(MotorsController):
    def __init__(self, io, motors, sync_freq=50.):
        MotorsController.__init__(self, io, motors, sync_freq)

        self.ids = [m.id for m in motors]


class _DxlRegisterController(_DxlController):
    def __init__(self, io, motors, sync_freq,
                 mode, regname, varname=None):
        _DxlController.__init__(self, io, motors, sync_freq)

        self.mode = mode
        self.regname = regname
        self.varname = regname if varname is None else varname

    def setup(self):
        if self.mode == 'set':
            self.get_register()

    def update(self):
        self.get_register() if self.mode == 'get' else self.set_register()

    def get_register(self):
        """ Gets the value from the specified register and sets it to the :class:`~pypot.dynamixel.motor.DxlMotor`. """
        motors = [m for m in self.motors if hasattr(m, self.varname)]
        if not motors:
            return
        ids = [m.id for m in motors]

        values = getattr(self.io, 'get_{}'.format(self.regname))(ids)
        for m, val in zip(motors, values):
            m.__dict__[self.varname] = val

    def set_register(self):
        """ Gets the value from :class:`~pypot.dynamixel.motor.DxlMotor` and sets it to the specified register. """
        motors = [m for m in self.motors if hasattr(m, self.varname)]

        if not motors:
            return
        ids = [m.id for m in motors]

        values = (m.__dict__[self.varname] for m in motors)
        getattr(self.io, 'set_{}'.format(self.regname))(dict(zip(ids, values)))


class _PosSpeedLoadDxlController(_DxlController):
    def setup(self):
        torques = self.io.is_torque_enabled(self.ids)
        for m, c in zip(self.motors, torques):
            m.compliant = not c
        self._old_torques = torques

        values = self.io.get_goal_position_speed_load(self.ids)
        positions, speeds, loads = zip(*values)
        for m, p, s, l in zip(self.motors, positions, speeds, loads):
            m.__dict__['goal_position'] = p
            m.__dict__['moving_speed'] = s
            m.__dict__['torque_limit'] = l
            
        

    def update(self):
        self.get_present_position_speed_load()
        self.set_goal_position_speed_load()

    def get_present_position_speed_load(self):
        values = self.io.get_present_position_speed_load(self.ids)

        if not values:
            return

        positions, speeds, loads = zip(*values)
        
        #For Dynaban :
        copyNexts = self.io.get_copy_next_buffer(self.ids)
        #Attention !
        torques = self.ids#self.io.get_outputTorque(self.ids)

        for m, p, s, l, b, t in zip(self.motors, positions, speeds, loads, copyNexts, torques):
            m.__dict__['present_position'] = p
            m.__dict__['present_speed'] = s
            m.__dict__['present_load'] = l
            m.__dict__['copy_next_buffer_read'] = b
            m.__dict__['updated'] = True
            m.__dict__['output_torque'] = t
        
        

    def set_goal_position_speed_load(self):
        change_torque = {}
        torques = [not m.compliant for m in self.motors]
        for m, t, old_t in zip(self.motors, torques, self._old_torques):
            if t != old_t:
                change_torque[m.id] = t
        self._old_torques = torques
        if change_torque:
            self.io._set_torque_enable(change_torque)

        rigid_motors = [m for m in self.motors if not m.compliant]
        ids = tuple(m.id for m in rigid_motors)

        if not ids:
            return

        values = ((m.__dict__['goal_position'],
                   m.__dict__['moving_speed'],
                   m.__dict__['torque_limit']) for m in rigid_motors)
        self.io.set_goal_position_speed_load(dict(zip(ids, values)))
        
        
        # For Dynaban
        
#         for m in self.motors :
#             if (m.mode_dynaban == 1 or m.mode_dynaban == 2 or m.mode_dynaban == 3) :
#                 #We're following a trajectory, we'll make sure that when it ends, the goal position is where the motor stands
#                 m.goal_position = m.present_position
        
        next_buffer_motors = []
        for m in self.motors :
            if (m.send_copy_next_buffer_write) :
                next_buffer_motors.append(m)
                m.send_copy_next_buffer_write = False
        values = ((m.__dict__['copy_next_buffer_write']) for m in next_buffer_motors)
        ids = tuple(m.id for m in next_buffer_motors)
        self.io.set_copy_next_buffer(dict(zip(ids, values)))
        
        mode_motors = []
        for m in self.motors :
            if (m.send_mode) :
                mode_motors.append(m)
                m.send_mode = False
                
        values = ((m.__dict__['mode_dynaban']) for m in mode_motors)
        ids = tuple(m.id for m in mode_motors)
        self.io.set_mode_dynaban(dict(zip(ids, values)))
        
        #The mode will be sent the next tick (usualy to make sure the fiels are already read by the firmware before changing the mode)
        for m in self.motors :
            if (m.send_mode_delayed) :
                m.send_mode = True
                m.send_mode_delayed = False
         
        # Hack to add support for the Dynaban trajectories :
        #Buffer 1 :
        traj_motors1 = [m for m in self.motors if m.update_buffer1 ]
        
        if (len(traj_motors1) != 0) :
            #Only the motors flaged with the update_buffer will be updated
            ids = tuple(m.id for m in traj_motors1)
        
            values = ((m.__dict__['traj1_size']) for m in traj_motors1)
            self.io.set_traj1_size(dict(zip(ids, values)))
            values = ((m.__dict__['a0_traj1']) for m in traj_motors1)
            self.io.set_a0_traj1(dict(zip(ids, values)))
            values = ((m.__dict__['a1_traj1']) for m in traj_motors1)
            self.io.set_a1_traj1(dict(zip(ids, values)))
            values = ((m.__dict__['a2_traj1']) for m in traj_motors1)
            self.io.set_a2_traj1(dict(zip(ids, values)))
            values = ((m.__dict__['a3_traj1']) for m in traj_motors1)
            self.io.set_a3_traj1(dict(zip(ids, values)))
            values = ((m.__dict__['a4_traj1']) for m in traj_motors1)
            self.io.set_a4_traj1(dict(zip(ids, values)))
            values = ((m.__dict__['torque1_size']) for m in traj_motors1)
            self.io.set_torque1_size(dict(zip(ids, values)))
            values = ((m.__dict__['a0_torque1']) for m in traj_motors1)
            self.io.set_a0_torque1(dict(zip(ids, values)))
            values = ((m.__dict__['a1_torque1']) for m in traj_motors1)
            self.io.set_a1_torque1(dict(zip(ids, values)))
            values = ((m.__dict__['a2_torque1']) for m in traj_motors1)
            self.io.set_a2_torque1(dict(zip(ids, values)))
            values = ((m.__dict__['a3_torque1']) for m in traj_motors1)
            self.io.set_a3_torque1(dict(zip(ids, values)))
            values = ((m.__dict__['a4_torque1']) for m in traj_motors1)
            self.io.set_a4_torque1(dict(zip(ids, values)))
            values = ((m.__dict__['duration1']) for m in traj_motors1)
            self.io.set_duration1(dict(zip(ids, values)))
 
        #Buffer 2 :
        traj_motors2 = [m for m in self.motors if m.update_buffer2 ]
        if (len(traj_motors2) != 0) :
            #Only the motors flaged with the update_buffer will be updated
            ids = tuple(m.id for m in traj_motors2)
             
            values = ((m.__dict__['traj2_size']) for m in traj_motors2)
            self.io.set_traj2_size(dict(zip(ids, values)))
            values = ((m.__dict__['a0_traj2']) for m in traj_motors2)
            self.io.set_a0_traj2(dict(zip(ids, values)))
            values = ((m.__dict__['a1_traj2']) for m in traj_motors2)
            self.io.set_a1_traj2(dict(zip(ids, values)))
            values = ((m.__dict__['a2_traj2']) for m in traj_motors2)
            self.io.set_a2_traj2(dict(zip(ids, values)))
            values = ((m.__dict__['a3_traj2']) for m in traj_motors2)
            self.io.set_a3_traj2(dict(zip(ids, values)))
            values = ((m.__dict__['a4_traj2']) for m in traj_motors2)
            self.io.set_a4_traj2(dict(zip(ids, values)))
            values = ((m.__dict__['torque2_size']) for m in traj_motors2)
            self.io.set_torque2_size(dict(zip(ids, values)))
            values = ((m.__dict__['a0_torque2']) for m in traj_motors2)
            self.io.set_a0_torque2(dict(zip(ids, values)))
            values = ((m.__dict__['a1_torque2']) for m in traj_motors2)
            self.io.set_a1_torque2(dict(zip(ids, values)))
            values = ((m.__dict__['a2_torque2']) for m in traj_motors2)
            self.io.set_a2_torque2(dict(zip(ids, values)))
            values = ((m.__dict__['a3_torque2']) for m in traj_motors2)
            self.io.set_a3_torque2(dict(zip(ids, values)))
            values = ((m.__dict__['a4_torque2']) for m in traj_motors2)
            self.io.set_a4_torque2(dict(zip(ids, values)))
            values = ((m.__dict__['duration2']) for m in traj_motors2)
            self.io.set_duration2(dict(zip(ids, values)))
         
        for m in self.motors :
            # Resetting the update_buffer flags
            m.update_buffer1 = False
            m.update_buffer2 = False
        